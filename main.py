import csv
import random

list1 = [] # Create first list
list2 = [] # Create second list

list1_only = []

#test_list1 = [1,2,3,4,5,6,7,8,9,10]
#test_list2 = [10,11,12,13,14,15,16,17,18]


# load csv file entries into an array
def load_csv_to_array(_csv, _list):
    with open(_csv) as csvfile:
        users = csv.reader(csvfile)
        for user in users:
            _list.append('\n'.join(user))

# Load information into first list
load_csv_to_array('list1.csv', list1)
# Load information into second list
load_csv_to_array('list2.csv', list2)


# Save the array to a file
def save_array_to_file(_file, _list):
    f = open(_file, "w+")

    for i in range(0,_list.__len__()):
        f.write("%s\r\n" % (_list[i]))

    f.close()

# Can use specific csv writer
def save_array_to_csv(_csv, _list):
    with open(_csv , 'w') as f:
        writer = csv.writer(f)
        writer.writerow(_list)

    f.close()
    print ('saved ' + _csv)

# Simply counts the amount of duplicates in two lists
def count_dupes(_list1, _list2):
    in_both = 0

    for u1 in _list1:
        for u2 in _list2:
            if (u1 == u2):
                in_both = in_both + 1

    return in_both

# Displays how many unique entries are in the first list and not in the second
def count_only_in_first(_list1, _list2):
    in_first = 0

    for u1 in _list1:
        if (u1 in _list2):
            pass
        else:
            in_first = in_first + 1
            #print(u1)

    return in_first

# Saves unique entries from the first list that aren't in the second list to a new array
def get_new_list_from_unique_entries(_list1, _list2, _out):
    in_first = 0

    for u1 in _list1:
        if (u1 in _list2):
            pass
        else:
            in_first = in_first + 1
            _out.append(u1)

    return _out


# Copies random _amount of entries _from one list _to another
def inject_random(_from, _to, _amount):
    _temp = _from[:]

    count = 0;

    if (_amount <= _temp.__len__()):
        for i in range(_amount):
            rnd = random.randrange(0, _temp.__len__())
            _to.append(_temp[rnd])
            _temp.pop(rnd)
            count = count + 1

# Get a third list with the entries we've requested
get_new_list_from_unique_entries(list1, list2, list1_only)

# Save results from third list
save_array_to_file('list1_only.csv', list1_only)